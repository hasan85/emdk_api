from django.contrib import admin
from .models import VolunteerImage, Field, Services, Apply


class FieldNameInline(admin.TabularInline):
	model = Field
	fields = ("field_name",)


class ServicesAdmin(admin.ModelAdmin):
	list_display = ("service_name",)
	inlines = [FieldNameInline]


class ApplyAdmin(admin.ModelAdmin):
	list_display = ("title", "last_exec_date", "status", "created_at", "updated_at")


# Register your models here.
admin.site.register(VolunteerImage)
admin.site.register(Services, ServicesAdmin)
admin.site.register(Apply, ApplyAdmin)
