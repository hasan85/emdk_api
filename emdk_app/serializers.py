import json

def serialize_dict_to_json(_dict):
    temporary_dict = dict()
    for field in _dict:
        temporary_dict[field] = _dict[field]
    return json.dumps(temporary_dict, indent=2, ensure_ascii=False)


def serialize_list_to_json(_list):
    items = list()
    for index, item in enumerate(_list):
        for field in item:
            temp_json[field] = str(item[field])
        items.append(temp_json)
        items_json = json.dumps(items, ensure_ascii=False)
        return items_json
    return None
