from django.db import models
from django.conf import settings
from base_user.models import Cuser
from django.contrib.auth.models import BaseUserManager
import datetime
# import secrets
import os
import hashlib

STATUS_VARS = (("pending", "PENDING"), ("aproved", "APROVED"), ("rejected", "REJECTED"),)


class VolunteerImage(models.Model):
	volunteer_photo = models.ImageField(verbose_name="Konullunun sekili")


class Services(models.Model):
	service_name = models.CharField(max_length=255, verbose_name="Service name", blank=False, null=False)


class Field(models.Model):
	field_name = models.CharField(max_length=255, verbose_name="Field name", blank=False, null=False)
	service_key = models.ForeignKey("Services", on_delete=models.CASCADE)


class Apply(models.Model):
	title = models.CharField(max_length=255)
	reason_id = models.IntegerField(default=0)
	last_exec_date = models.DateField(default=datetime.date.today, null=True, blank=True)
	status = models.CharField(choices=STATUS_VARS, max_length=255, default=STATUS_VARS[0])
	user = models.ForeignKey("base_user.Cuser", on_delete=models.CASCADE)
	notification_status = models.BooleanField(default=False)
	# logs
	created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
	updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
	def as_dict( self ):
		return {
			"title": self.title,
			"last_exec_date": self.last_exec_date,
			"reason_id": self.reason_id,
			"status": self.status,
			"notification_status": self.notification_status,
		}

	def change_status( self, status ):
		if status in STATUS_VARS:
			self.status = status
		else:
			return None

	class Meta:
		verbose_name = "Elektron müraciət"
		verbose_name_plural = "Elektron xidmətlər"
		ordering = ("-last_exec_date",)
