import json
from rest_framework.decorators import api_view
from django.http import JsonResponse, HttpResponse
from zeep import Client
from .serializers import serialize_dict_to_json, serialize_list_to_json
import hashlib
import os
from base_user.models import Cuser
from django.contrib.auth import authenticate, login, logout
from pyfcm import FCMNotification
from emdk_app.models import Services, Apply
import requests

url = "http://31.170.236.5:30045/MobileService.svc?wsdl"
client = Client(wsdl=url)


@api_view(['GET', 'POST'])
def test( request ):
	return JsonResponse({"message": "helloworld"})


@api_view(['GET'])
def get_auctions( request, type ):
	response = client.service.WS_GetAuctionInfo(type)
	if response.itemList:
		auctions = response.itemList.Auctions
	else:
		auctions = {}
	auctions_json = serialize_list_to_json(auctions) if auctions else ([])
	return _jsonResponse(auctions_json)


@api_view(['POST'])
def add_volunteer( request ):
	volunteer_data = request.data
	response = client.service.WS_AddVolunteerInfo(volunteer_data)
	return _jsonResponse({"status": response.WS_AddVolunteerInfoResult.ResultMessage})


@api_view(['GET'])
def get_news_detail( request, news_id ):
	news_response = client.service.WS_GetNewsInfoByID(news_id)
	# print("news_response", news_response.itemList)
	news_item = news_response.itemList
	fields = dir(news_item)
	article_json = {}
	for field in fields:
		article_json[field] = news_item[field]
	return _jsonResponse(article_json)


@api_view(["GET"])
def get_news( request, page ):
	news_response = client.service.WS_GetNewsInfo(lang="az", page=page)
	status_code = news_response.WS_GetNewsInfoResult.ResultCode
	if status_code == 4:
		return _jsonResponse({
			"status_code": status_code,
			"status": "Gözlənilməz server xətası"
		})
	news_items = news_response.itemList.News
	news_items_json = []
	for index, item in enumerate(news_items):
		fields = dir(item)
		temp_json = {}
		for field in fields:
			temp_json[field] = item[field]
		news_items_json.append(temp_json)
	return _jsonResponse({"status_code": "1", "data": news_items_json})


@api_view(["POST"])
def online_register( request ):
	register_data = request.data
	response = client.service.WS_OnlineQeydiyyat(register_data)
	return _jsonResponse({"status_code": response.ResultCode, "status": response.ResultMessage})


@api_view(["POST"])
def complain( request ):
	data = request.data
	response = client.service.WS_OnlineShikayet(data)
	return _jsonResponse({"status_code": response.ResultCode, "status": response.ResultMessage})


# {
#     "username":"mahwd",
#     "password": "pass"
# }


@api_view(["POST"])
def login_user( request ):
	# if request.method == "POST":
	creditials = request.data
	username = creditials["username"]
	password = creditials["password"]
	_user = authenticate(request, username=username, password=password)
	if _user:
		login(request=request, user=_user)
		user = Cuser.get_by_username(username=username)
		if user:
			response = {
				"username": user.get_username(),
				"name": user.first_name,
				"surname": user.last_name,
				"father_name": user.father_name,
				"email": user.email,
				"contact_phone": user.contact_phone,
				"current_address": user.current_address,
				"password_reset_phone": user.password_reset_phone,
				"token": user.auth_token,
			}
			return _jsonResponse({
				"status_code": "1",
				"data": response
			})
	else:
		return _jsonResponse({
			"status_code": "4",
			"status": "Failed to authenticate"
		})


# else:
# 	return _jsonResponse({
# 		"status_code": "4",
# 		"status": "Not Allowed request method : < %s >" % request.method
# 	})


@api_view(["POST", "GET"])
def register_user( request ):
	if request.method == "POST":
		creditials = request.data
		username = creditials.get("username", "")
		email = creditials.get("email", "")
		password = creditials.get("password", "")
		name = creditials.get("name", "")
		surname = creditials.get("surname", "")
		father_name = creditials.get("father_name", "")
		user = None
		if username and password:
			user = Cuser.objects.create_user(username=username, password=password)
		if user:
			if email:
				user.email = email
			if name:
				user.first_name = name
			if surname:
				user.last_name = surname
			if father_name:
				user.father_name = father_name
			user.save()
		_user = authenticate(request=request, username=username, password=password)
		if _user:
			login(request=request, user=_user)
		if user:
			response = {
				"username": user.get_username(),
				"name": user.first_name,
				"surname": user.last_name,
				"father_name": user.father_name,
				"email": user.email,
				"contact_phone": user.contact_phone,
				"current_address": user.current_address,
				"password_reset_phone": user.password_reset_phone,
				"token": user.auth_token,
			}
			return _jsonResponse({
				"status_code": "1",
				"data": response
			})
		else:
			return _jsonResponse({
				"status_code": "4",
				"status": "Failed to register"
			})
	else:
		return _jsonResponse({
			"status_code": "4",
			"status": "Not Allowed request method : < %s >" % request.method
		})


@api_view(["POST"])
def check_token( req ):
	data = req.data
	token = data.get("token")
	if token:
		user = Cuser.get_by_token(token=token)
		if user:
			_user = {
				"username": user.get_username(),
				"name": user.first_name,
				"surname": user.last_name,
				"father_name": user.father_name,
				"email": user.email,
				"contact_phone": user.contact_phone,
				"current_address": user.current_address,
				"password_reset_phone": user.password_reset_phone,
				"token": user.auth_token,
			}
			return _jsonResponse({"status_code": "1", "data": _user})
		else:
			return _jsonResponse({"status_code": "2", "status": "No users with such token."})
	else:
		return _jsonResponse({"status_code": "4", "status": "invalid token"})


@api_view(["GET"])
def log_out( req ):
	logout(request=req)
	return _jsonResponse({"status_code": "1", "status": "logged out"})


@api_view(["POST"])
def update_profile( req ):
	data = req.data
	token = data.get("token", "")
	email = data.get("email", "")
	contact_phone = data.get("contact_phone", "")
	current_address = data.get("current_address", "")
	password_reset_phone = data.get("password_reset_phone", "")
	if token:
		user = Cuser.get_by_token(token=token)
		response_user = {}
		if user:
			if email:
				user.email = email
				response_user["email"] = email
			if contact_phone:
				user.contact_phone = contact_phone
				response_user["contact_phone"] = contact_phone
			if current_address:
				user.current_address = current_address
				response_user["current_address"] = current_address
			if password_reset_phone:
				user.password_reset_phone = password_reset_phone
				response_user["password_reset_phone"] = password_reset_phone
			user.save()
			return _jsonResponse({"status_code": "1", "status": "changes done", "data": response_user})
		else:
			return _jsonResponse({"status_code": "4", "status": "No users with such token."})
	return _jsonResponse({"status_code": "4", "status": "missing token"})


@api_view(["GET"])
def getLotsByAuctionId( req, auc_id ):
	response = client.service.WS_GetLotsByAuctionId(auc_id)
	if response.WS_GetLotsByAuctionIdResult.ResultCode == 1:
		if response.itemList:
			lots = response.itemList.LotsDetail
		else:
			lots = {}
		lots_json = serialize_list_to_json(lots) if lots else ([])
		return _jsonResponse(lots_json)
	else:
		return _jsonResponse({"status_code": response.WS_GetLotsByAuctionIdResult.ResultCode,
		                      "stsatus": response.WS_GetLotsByAuctionIdResult.ResultMessage})


@api_view(["GET"])
def getRegions( req ):
	response = client.service.WS_GetRegionsLevel01()
	status_code = response.WS_GetRegionsLevel01Result.ResultCode
	status = response.WS_GetRegionsLevel01Result.ResultMessage
	if status_code == 1:
		regions = serialize_list_to_json(response.item.Region) if response.item else ([])
		return _jsonResponse({"status_code": status_code, "data": regions})
	else:
		return _jsonResponse({"status_code": status_code, "stsatus": status})


@api_view(["POST"])
def getNotification( req ):
	notificationData = req.data
	registration_id = notificationData.get("fcm_token", "")
	message_title = notificationData.get("message_tite", "")
	message_body = notificationData.get("message_body", "")
	push_service = FCMNotification(api_key="AIzaSyAwqbAGwHb595otFtMJCMHvjpGgP3lIqcQ")
	result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title,
	                                           message_body=message_body)
	return _jsonResponse(result)


@api_view(["POST"])
def getPurposePeriod( req ):
	data = req.data
	reason_code = data.get("reason_code", "")
	if reason_code:
		_res = client.service.WS_GetPurposePeriod(reason_code)
		result = _res.WS_GetPurposePeriodResult
		if result.ResultCode == 1 and _res.item:
			return _jsonResponse({"status_code": result.ResultCode, "data": _res.item})
		else:
			return _jsonResponse({"status_code": result.ResultCode, "status": result.ResultMessage})


def validate( data, keys ):
	errors = {}
	for key in keys:
		if not data.get(key, None):
			errors[key] = "this field is required!"
	return errors if errors else True


@api_view(["POST"])
def sendApplication( req ):
	data = req.data
	creditials = data.get("creditials", "")
	service_name = data.get("service_name", "")
	user_token = data.get("user_token", "")
	reason_id = creditials.get('reasonCode', "")

	if user_token or service_name:
		user = Cuser.get_by_token(token=user_token)
		if not user:
			return _jsonResponse({"status_code": 4, "status": "invalid egov token"})
	else:
		return _jsonResponse({"status_code": 4, "status": "token is required!"})
	res = client.service.WS_SendApplication(creditials)
	resultCode = res.WS_SendApplicationResult.ResultCode
	resultMessage = res.WS_SendApplicationResult.ResultMessage
	response_data = res.item
	if resultCode == 1:
		apply = Apply.objects.create(title=service_name, last_exec_date=response_data.Son_Icra_Tarixi, status="PENDING",
		                             reason_id=reason_id, user_id=user.id)
		applyJson = {
			"title": apply.title,
			"last_exec_date": apply.last_exec_date,
			"status": apply.status,
			"reason_id": apply.reason_id,
			"notification_status": apply.notification_status,
		}
		return _jsonResponse({"status_code": resultCode, "data": serialize_dict_to_json(applyJson)})
	else:
		return _jsonResponse({"status_code": resultCode, "status": resultMessage})


@api_view(["GET"])
def getCookie( req ):
	return _jsonResponse({"cookies": req.COOKIES})


@api_view(["POST"])
def getApplies( req ):
	data = req.data
	if data:
		user_token = data.get("user_token", "")
		if user_token:
			user = Cuser.get_by_token(user_token)
			if user:
				apps = Apply.objects.filter(user_id=user.id)
				return _jsonResponse({"status_code": 1, "data": [app.as_dict() for app in apps]})
			else:
				return _jsonResponse({"status_code": 4, "status": "no such user"})

	else:
		return _jsonResponse({"status_code": 4, "status": "requires user token"})


def _jsonResponse( res ):
	return HttpResponse(json.dumps(res, ensure_ascii=False),
	                    content_type='application/json; charset=utf-8')
