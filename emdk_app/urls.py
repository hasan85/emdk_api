from .views import get_auctions, add_volunteer, get_news_detail, get_news, login_user, register_user, check_token, \
	log_out, update_profile, online_register, complain, getLotsByAuctionId, getRegions, getNotification, \
	getPurposePeriod, sendApplication, getApplies, getCookie
from django.urls import path

urlpatterns = [
	path('auctions/<int:type>', get_auctions, name="auctions"),
	path('add-volunteer/', add_volunteer, name="add-volunteer"),
	path('news/<int:page>', get_news, name="news"),
	path('article/<int:news_id>', get_news_detail, name="news-detail"),
	path('login', login_user, name='login'),
	path('register', register_user, name='register'),
	path('update-profile', update_profile, name='update-profile'),
	path('check-token', check_token, name='check-token'),
	path('logout', log_out, name='logout'),
	path('register-online', online_register, name='register-online'),
	path('complain', complain, name='complain'),
	path('lots/<int:auc_id>', getLotsByAuctionId, name="lots"),
	path('regions', getRegions, name="regions"),
	path('notification', getNotification, name="get-notification"),
	path('get-purpose-period', getPurposePeriod, name="get-purpose-period"),
	path('send-application', sendApplication, name="send-application"),
	path('get-applies', getApplies, name="get-applies"),
	path('get-cookies', getCookie, name="get-cookies"),
]
