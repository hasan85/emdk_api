# Generated by Django 2.1.1 on 2018-10-20 13:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('emdk_app', '0004_auto_20181020_0538'),
    ]

    operations = [
        migrations.AddField(
            model_name='apply',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='apply',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
