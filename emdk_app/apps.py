from django.apps import AppConfig


class EmdkAppConfig(AppConfig):
    name = 'emdk_app'
