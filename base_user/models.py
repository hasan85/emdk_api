from django.db import models
from django.utils import timezone
from django.conf import settings
from django.core import validators
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
from base_user.tools.common import get_user_profile_photo_file_name, GENDER, current_month_range, current_week_range
from rest_framework.authtoken.models import Token
import secrets

# Create your models here.
USER_MODEL = settings.AUTH_USER_MODEL


# Customize User model
class Cuser(AbstractBaseUser, PermissionsMixin):
	"""
	An abstract base class implementing a fully featured User model with
	admin-compliant permissions.

	Username, password and email are required. Other fields are optional.
	"""

	username = models.CharField(_('username'), max_length=100, unique=True,
	                            help_text=_('Tələb olunur. 75 simvol və ya az. Hərflər, Rəqəmlər və '
	                                        '@/./+/-/_ simvollar.'),
	                            validators=[
		                            validators.RegexValidator(r'^[\w.@+-]+$', _('Düzgün istifadəçi adı daxil edin.'),
		                                                      'yanlışdır')
	                            ])
	first_name = models.CharField(_('first name'), max_length=50, blank=True)
	last_name = models.CharField(_('last name'), max_length=50, blank=True)
	email = models.EmailField(_('email address'), max_length=255)
	profile_picture = models.ImageField(_('profile picture'), upload_to=get_user_profile_photo_file_name, null=True,
	                                    blank=True)
	gender = models.IntegerField(_('gender'), choices=GENDER, null=True, blank=True)
	contact_phone = models.IntegerField(_('contact phone'), null=True, blank=True)
	password_reset_phone = models.IntegerField(_('password reset phone'), null=True, blank=True)
	current_address = models.CharField(_('current address'), max_length=255, null=True, blank=True)
	father_name = models.CharField(_("father name"), max_length=120, null=True, blank=True)
	# ----------------------------------------------------------------------------------------------------
	auth_token = models.CharField(max_length=255, default='')
	# social_share = models.BooleanField(default=False, help_text="Sosial şəbəkədə paylaşıb paylaşmadığını bildirir")
	# freezing_chance = models.IntegerField(default=5, help_text="Hər bir istifadəçinin dondurma şansı")
	# bonus_chance = models.IntegerField(default=0, help_text="Hər bir istifadəçinin veriləm əlavə şans bonuslar")
	# unlimited = models.BooleanField(default=False, help_text="Istifadəçiyə istənilən qədər dondurmaq haqqı verir")

	is_staff = models.BooleanField(_('staff status'), default=False,
	                               help_text=_('Designates whether the user can log into this admin '
	                                           'site.'))
	is_active = models.BooleanField(_('active'), default=True,
	                                help_text=_('Designates whether this user should be treated as '
	                                            'active. Unselect this instead of deleting accounts.'))
	date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

	"""
		Important non-field stuff
	"""
	objects = UserManager()

	USERNAME_FIELD = 'username'
	REQUIRED_FIELDS = ['email']

	def save( self, *args, **kwargs ):
		if not self.auth_token:
			self.auth_token = secrets.token_hex(16)
		super(Cuser, self).save(*args, **kwargs)

	@classmethod
	def get_by_username( cls, username ):
		u = cls.objects.filter(username=username)
		if u.count() >= 1:
			return u.first()
		else:
			return None

	@classmethod
	def get_by_token( cls, token ):
		u = cls.objects.filter(auth_token=token)
		if u.count() >= 1:
			return u.first()
		else:
			return None

	class Meta:
		verbose_name = 'İstifadəçi'
		verbose_name_plural = 'İstifadəçilər'

	def get_full_name( self ):
		"""
			Returns the first_name plus the last_name, with a space in between.
		"""
		full_name = '%s %s' % (self.first_name, self.last_name)
		return full_name.strip()

	def get_short_name( self ):
		"""
			Returns the short name for the user.
		"""
		return self.first_name

	# def get_user_friends(self):
	#     """
	#         This mehod return user friends
	#     """
	#     try:
	#         facebook_user = self.social_auth.get(provider='facebook')
	#         access_token = facebook_user.get_access_token("facebook")  # get user access token
	#         facebook_uuid = facebook_user.uid
	#         graph = facebook.GraphAPI(access_token)
	#         resp = graph.get_object(facebook_uuid + '/friends')  # get friends data
	#         data = resp['data']
	#         return data if data != [] else None
	#     except:
	#         return None
	#
	# def get_avatar(self):
	#     if self.profile_picture:
	#         try:
	#             return "https://graph.facebook.com/%s/picture?type=large" % self.social_auth.get(provider='facebook').uid
	#         except:
	#             return "https://graph.facebook.com/%s/picture?type=large" % '100002461198950'
	#     else:
	#         return "https://graph.facebook.com/%s/picture?type=large" % '100002461198950'
	#
	# # def last_game_result(self):
	#     try:
	#         game = self.gameobject_set.all().first()
	#         result = {}
	#         result['point'] = game.point
	#         result['duration'] = game.duration
	#         return result
	#     except:
	#         return []

	def get_token( self ):
		"""
			Get Rest framework token if exist
			:return: token
		"""
		try:
			token = Token.objects.get(user=self)
			return token.key
		except:
			return "None"
