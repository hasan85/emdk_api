from rest_framework import serializers
from emdk_app.models import VolunteerImage


class VolunteerImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = VolunteerImage
        fields = ('pk', 'volunteer_photo')
