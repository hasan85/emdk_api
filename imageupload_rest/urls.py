from django.urls import path, include
from rest_framework import routers
from .viewsets import VolunteerImageViewset
router = routers.DefaultRouter()
router.register("imagez", VolunteerImageViewset, 'images')

urlpatterns = [
    path('', include(router.urls))
]
