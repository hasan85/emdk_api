from rest_framework import viewsets
from .serializers import VolunteerImageSerializer
from emdk_app.models import VolunteerImage


class VolunteerImageViewset(viewsets.ModelViewSet):
    queryset = VolunteerImage.objects.all()
    serializer_class = VolunteerImageSerializer
